resource "aws_vpc" "main_vpc" {
  cidr_block           = "192.168.50.0/24"
  enable_dns_hostnames = false
  enable_dns_support   = true

  tags = {
    Name = "vpc-provisionning-pmn"
  }
}
