resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main_vpc.id

  tags = {
    # vpc-[nom_projet]-[nom-equipe]
    Name = "igw-provisionning-pmn"
  }
}

resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.subnet_public.id
  depends_on    = [aws_internet_gateway.igw]
  tags = {
    Name = "ngw-provisionning-pmn"
  }
}

resource "aws_eip" "nat_eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.igw]

  tags = {
    Name = "neip-provisionning-pmn"
  }
}