locals {
  ssh_config_rendered_content = templatefile("../../templates/ssh_config.tmpl", {
    s0_public_ip = data.aws_instance.data_s0.public_ip
  })
}

resource "null_resource" "local1" {
  triggers = {
    template = local.ssh_config_rendered_content
  }
  provisioner "local-exec" {
    command = format("cat <<\"EOF\" > \"%s\"\n%s\nEOF", "../../ansible_config/ssh.cfg", local.ssh_config_rendered_content)
  }
}
