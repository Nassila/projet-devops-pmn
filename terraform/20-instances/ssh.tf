resource "aws_key_pair" "ssh_terraform" {
  key_name   = "ssh_terraform"
  public_key = file("~/.ssh/id_rsa_aws.pub")
}