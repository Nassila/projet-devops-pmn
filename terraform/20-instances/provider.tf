provider "aws" {
  profile = "default"
  region  = "eu-west-3"
}

terraform {
  required_version = "1.0.6"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.54.0"
    }
  }

  backend "s3" {
    encrypt        = true
    bucket         = "storage-tfstate-devops"
    dynamodb_table = "terraform-state-lock-dynamo"
    region         = "eu-west-3"
    key            = "instance/terraform.tfstate"
  }
}
