data "aws_vpc" "data_main_vpc" {
  filter {
    name   = "tag:Name"
    values = ["vpc-provisionning-pmn"]
  }
}

data "aws_subnet" "data_subnet_public" {
  filter {
    name   = "tag:Name"
    values = ["subnet-public-provisionning-pmn"]
  }
}

data "aws_subnet" "data_subnet_private" {
  filter {
    name   = "tag:Name"
    values = ["subnet-private-provisionning-pmn"]
  }
}