output "s0_ip_public" {
  value = aws_instance.s0_infra.public_ip
}

output "data_vpc_id" {
  value = data.aws_vpc.data_main_vpc.id
}

output "data_subnet_public_id" {
  value = data.aws_subnet.data_subnet_public.id
}

output "data_subnet_private_id" {
  value = data.aws_subnet.data_subnet_private.id
}