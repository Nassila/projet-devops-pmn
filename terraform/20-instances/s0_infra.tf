resource "aws_instance" "s0_infra" {
  ami           = "ami-00662eead74f66895"
  instance_type = "t2.micro"
  key_name      = aws_key_pair.ssh_terraform.key_name

  network_interface {
    network_interface_id = aws_network_interface.s0_network_interface.id
    device_index         = 0
  }

  tags = {
    Name = "s0-infra-provisionning-pmn"
  }
}

resource "aws_network_interface" "s0_network_interface" {
  subnet_id       = data.aws_subnet.data_subnet_public.id
  private_ips     = ["192.168.50.5"]
  security_groups = [aws_security_group.sg_s0.id]

  tags = {
    Name = "s0-network-interface-provisionning-pmn"
  }
}

resource "aws_eip" "s0_eip" {
  vpc        = true
  instance = aws_instance.s0_infra.id
  tags = {
    Name = "s0-eip-provisionning-pmn"
  }
}