# Projet-DevOps-PMN-Provisionning
_(Ghezal Ilies, Myriam wasfi, Nassila hamouche)_

This project aims at the realization of a high availability web hosting infrastructure with the methods and tools seen during the course.

During this project, you will have to :

- set up infrastructure an AWS with terraform
- configure and connect server to each other

## Getting started

### Prerequisites

- Docker
- Terraform
- Ansible

### Installation

For documentation you may tap `make` command.

Step 1: add your `Access key ID` & `Secret access key` AWS

``` 
➜ cp credentials.csv.dist credentials.csv
```
Setp 2: please make you sure havec docker deamon start

```
➜ make add_credentials
```

Step 3: for first start-up 

```
➜ make initialize
```
or 

```
➜ make start
```

### For demonstration 
Step 1: down s1 on aws and apply `fix_demo_un` for recreate instance and provisionning with terraform and ansible

```
➜ make fix_demo_un
```

Step 2: down s2 and s0 on aws and apply `fix_demo_deux` for recreate instance and provisionning with terraform and ansible

```
➜ make fix_demo_deux
```


## Authors

- Ghezal Ilies
- Myriam Wassfi
- Nassila Hamouche

